#ifndef __HELPER_H__
#define __HELPER_H__

#include "nemu.h"
#include "cpu/decode/operand.h"

/* All function defined with 'make_helper' return the length of the operation. */
#define make_helper(name) int name(swaddr_t eip)

static inline uint32_t instr_fetch(swaddr_t addr, size_t len) {
	return swaddr_read(addr, len, R_CS);
}

/* Instruction Decode and EXecute */
static inline int idex(swaddr_t eip, int (*decode)(swaddr_t), void (*execute) (void)) {
	/* eip is pointing to the opcode */
	int len = decode(eip + 1);
	execute();
	return len + 1;	// "1" for opcode
}

/* shared by all helper function */
extern Operands ops_decoded;

#define op_src (&ops_decoded.src)
#define op_src2 (&ops_decoded.src2)
#define op_dest (&ops_decoded.dest)

#include "x86-inc/mmu.h"
#include <setjmp.h>
extern jmp_buf jbuf;

static inline void raise_intr(uint8_t NO) {
	/* Trigger an interrupt/exception with ``NO''.
	 * That is, use ``NO'' to index the IDT.
	 */
	uint8_t size = 4;
	cpu.esp -= size;
	swaddr_write(cpu.esp, size, cpu.eflags.val, R_SS);
	cpu.esp -= size;
	swaddr_write(cpu.esp, size, cpu.cs.val, R_SS);
	cpu.esp -= size;
	swaddr_write(cpu.esp, size, cpu.eip, R_SS);
	
	uint32_t base = cpu.idtr.base;
	GateDesc gd;
	gd.offset_15_0 = lnaddr_read(base + (NO << 3), 2);
	gd.segment = lnaddr_read(base + (NO << 3) + 2, 2);
	gd.offset_31_16 = lnaddr_read(base + (NO << 3) +  6, 2);

	uint32_t si = gd.segment >> 3 << 3;

	base = cpu.gdtr.base;
	SegDesc sd;
	sd.limit_15_0 = lnaddr_read(base + si, 2);
	sd.base_15_0 = lnaddr_read(base + si + 2, 2);
	sd.base_23_16 = lnaddr_read(base + si + 4, 1);
	sd.base_31_24 = lnaddr_read(base + si + 7, 1);
	sd.limit_19_16 = lnaddr_read(base + si + 6, 1) & 0xf;

	cpu.cs.val = gd.segment;
	cpu.cs.cache.base = (((sd.base_31_24 << 8) + sd.base_23_16) << 8) + sd.base_15_0;
	cpu.cs.cache.limit = (sd.limit_19_16 << 4) + sd.limit_15_0;
	cpu.eip = (gd.offset_31_16 << 16) + gd.offset_15_0;
	
	/* Jump back to cpu_exec() */
	longjmp(jbuf, 1);
}


#endif
