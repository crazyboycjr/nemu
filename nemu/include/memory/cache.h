#ifndef __CACHE_H__
#define __CACHE_H__

void init_cache();
int in_L1_cache(hwaddr_t);
int in_L2_cache(hwaddr_t);
uint32_t cache_read(hwaddr_t, size_t);
void cache_write(hwaddr_t, size_t, uint32_t);

uint32_t dram_read(hwaddr_t, size_t);
void dram_write(hwaddr_t, size_t, uint32_t);
uint32_t my_dram_read(hwaddr_t, size_t);
void my_dram_write(hwaddr_t, size_t, uint32_t);

#define BLOCK_WIDTH 6


#define L1_CACHE_WIDTH 16
#define L1_WAYS_WIDTH 3
#define L1_SET_WIDTH (L1_CACHE_WIDTH - BLOCK_WIDTH - L1_WAYS_WIDTH)
#define L1_MARK_WIDTH (32 - L1_SET_WIDTH - BLOCK_WIDTH)

#define BLOCK_SIZE (1 << BLOCK_WIDTH)
#define L1_CACHE_SIZE (1 << L1_CACHE_WIDTH)
#define L1_WAYS_SIZE (1 << L1_WAYS_WIDTH)

#define L1_NR_ROWS (L1_CACHE_SIZE / BLOCK_SIZE / L1_WAYS_SIZE)

typedef union {
	struct {
		uint32_t offset	: BLOCK_WIDTH;
		uint32_t set	: L1_SET_WIDTH;
		uint32_t mark	: L1_MARK_WIDTH;
	};
	uint32_t addr;
} L1_cache_addr;

typedef struct L1_Row_Cache {
	uint8_t buf[BLOCK_SIZE];
	uint32_t mark;
	bool valid;
} L1_Row_Cache;



#define L2_CACHE_WIDTH 22
#define L2_WAYS_WIDTH 4
#define L2_SET_WIDTH (L2_CACHE_WIDTH - BLOCK_WIDTH - L2_WAYS_WIDTH)
#define L2_MARK_WIDTH (32 - L2_SET_WIDTH - BLOCK_WIDTH)

#define L2_CACHE_SIZE (1 << L2_CACHE_WIDTH)
#define L2_WAYS_SIZE (1 << L2_WAYS_WIDTH)

#define L2_NR_ROWS (L2_CACHE_SIZE / BLOCK_SIZE / L2_WAYS_SIZE)

typedef union {
	struct {
		uint32_t offset	: BLOCK_WIDTH;
		uint32_t set	: L2_SET_WIDTH;
		uint32_t mark	: L2_MARK_WIDTH;
	};
	uint32_t addr;
} L2_cache_addr;

typedef struct L2_Row_Cache {
	uint8_t buf[BLOCK_SIZE];
	uint32_t mark;
	bool valid, dirty;
} L2_Row_Cache;



#endif
