#ifndef __TLB_H__
#define __TLB_H__

//#define ENABLE_TLB


#include "common.h"
#define TLB_WIDTH 6

#define TLB_SIZE (1 << TLB_WIDTH)

typedef struct TLB_item {
	uint32_t tag;
	uint32_t page_frame;
	bool valid;
} TLB_item;

extern TLB_item tlbbufs[TLB_SIZE];

int in_TLB(lnaddr_t);
void substitude_TLB(uint32_t, uint32_t);
void init_tlb();

#endif

