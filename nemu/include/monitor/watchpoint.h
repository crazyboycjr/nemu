#ifndef __WATCHPOINT_H__
#define __WATCHPOINT_H__

#include "common.h"

typedef struct watchpoint {
	int NO;
	char expr[256];
	uint32_t cur_val;
	bool is_breakpoint;
	struct watchpoint *next;

} WP;

#endif
