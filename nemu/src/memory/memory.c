#include "common.h"
#include "cpu/reg.h"

extern CPU_state cpu;

uint32_t dram_read(hwaddr_t, size_t);
uint32_t my_dram_read(hwaddr_t, size_t);
void dram_write(hwaddr_t, size_t, uint32_t);
void my_dram_write(hwaddr_t, size_t, uint32_t);
uint32_t cache_read(hwaddr_t, size_t);
void cache_write(hwaddr_t, size_t, uint32_t);

//#define ENABLE_CACHE
#define ENABLE_PAGE

#include "memory/tlb.h"

/* Memory accessing interfaces */
#include "device/mmio.h"
extern MMIO_t maps[8];
__attribute__  ((gnu_inline)) inline uint32_t hwaddr_read(hwaddr_t addr, size_t len) {
	if (addr < maps[0].low || addr > maps[0].high) {
#ifdef ENABLE_CACHE
		return cache_read(addr, len) & (~0u >> ((4 - len) << 3));
#else
		return my_dram_read(addr, 4) & (~0u >> ((4 - len) << 3));
#endif
	} else {
		return mmio_read(addr, len, 0);
	}
}

void hwaddr_write(hwaddr_t addr, size_t len, uint32_t data) {
	if (addr < maps[0].low || addr > maps[0].high) {
#ifdef ENABLE_CACHE
		cache_write(addr, len, data);
#else
		my_dram_write(addr, len, data);
#endif
	} else {
		mmio_write(addr, len, data, 0);
	}
}

#ifdef ENABLE_PAGE
#include "x86-inc/mmu.h"
__attribute__  ((gnu_inline)) inline hwaddr_t page_translate(lnaddr_t addr) {
#ifdef ENABLE_TLB
	int idx = in_TLB(addr);
	if (idx >= 0) {
		return (tlbbufs[idx].page_frame << 12) + (addr & PAGE_MASK);
	}
#endif
	int dir, page, offset;
	dir = addr >> 22;
	page = addr >> 12 & 0x3ff;
	offset = addr & PAGE_MASK;
	
	PDE dir_item;
	dir_item.val = hwaddr_read((cpu.cr3.page_directory_base << 12) + (dir << 2), 4);
	assert(dir_item.present);
	
	PTE page_item;
	page_item.val = hwaddr_read((dir_item.page_frame << 12) + (page << 2), 4);
	assert(page_item.present);

#ifdef ENABLE_TLB
	substitude_TLB(addr >> 12, page_item.page_frame);
#endif
	return (page_item.page_frame << 12) + offset;
}
#endif


__attribute__  ((gnu_inline)) inline int32_t lnaddr_read(lnaddr_t addr, size_t len) {
#ifdef ENABLE_PAGE
	assert(len == 1 || len == 2 || len == 4);
	if (cpu.cr0.protect_enable && cpu.cr0.paging) {
		uint32_t offset = addr & PAGE_MASK;
		hwaddr_t hwaddr = page_translate(addr);
		uint32_t ret = hwaddr_read(hwaddr, len);
		
		if (offset + len > PAGE_SIZE) {
			/* this is a special case, you can handle it later. */
			hwaddr_t hwaddr2 = page_translate((addr + PAGE_SIZE) & ~PAGE_MASK);
			int shift = ((offset + len - PAGE_SIZE) << 3) - 1;
			uint32_t mask = ((1 << 31) >> shift);
			ret = (ret & ~mask) | ((hwaddr_read(hwaddr2, len) & (mask >> (31 - shift))) << (31 - shift));
		}
		return ret;
	} else {
		return hwaddr_read(addr, len);
	}
#else
	return hwaddr_read(addr, len);
#endif
}

void lnaddr_write(lnaddr_t addr, size_t len, uint32_t data) {
#ifdef ENABLE_PAGE
	assert(len == 1 || len == 2 || len == 4);
	if (cpu.cr0.protect_enable && cpu.cr0.paging) {
		uint32_t offset = addr & PAGE_MASK;
		int more_len = offset + len - PAGE_SIZE;
		hwaddr_t hwaddr = page_translate(addr);
		if (more_len > 0)
			hwaddr_write(hwaddr, 4 - more_len, data & (~0u >> ((more_len << 3) - 1)));
		else
			hwaddr_write(hwaddr, len, data);
		
		if (offset + len > PAGE_SIZE) {
			/* this is a special case, you can handle it later. */
			hwaddr_t hwaddr2 = page_translate((addr + PAGE_SIZE) & ~PAGE_MASK);
			int shift = (more_len << 3) - 1;
			uint32_t mask = ((1 << 31) >> shift);
			hwaddr_write(hwaddr2, more_len, (data & mask) >> ((4 - more_len) << 3));
		}
	} else {
		return hwaddr_write(addr, len, data);
	}
#else
	return hwaddr_write(addr, len);
#endif
}

inline lnaddr_t seg_translate(swaddr_t addr, size_t len, uint8_t sreg) {
	uint32_t seg_base = cpu.segr[sreg].cache.base;
	return seg_base + addr;
}


uint32_t swaddr_read(swaddr_t addr, size_t len, uint8_t sreg) {
#ifdef DEBUG
	assert(len == 1 || len == 2 || len == 4);
#endif
	if (cpu.cr0.protect_enable) {
		lnaddr_t lnaddr = seg_translate(addr, len, sreg);
		return lnaddr_read(lnaddr, len);
	} else {
		return lnaddr_read(addr, len);
	}
}

void swaddr_write(swaddr_t addr, size_t len, uint32_t data, uint8_t sreg) {
#ifdef DEBUG
	assert(len == 1 || len == 2 || len == 4);
#endif
	if (cpu.cr0.protect_enable) {
		lnaddr_t lnaddr = seg_translate(addr, len, sreg);
		return lnaddr_write(lnaddr, len, data);
	} else {
		lnaddr_write(addr, len, data);
	}
}
