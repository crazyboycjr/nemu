#include "common.h"
#include "misc.h"
#include "memory/cache.h"
#include <stdlib.h>

#define ENABLE_L2_CACHE

void ddr3_write(hwaddr_t, void*, uint8_t*);
L1_Row_Cache cachebufs[L1_NR_ROWS][L1_WAYS_SIZE];
L2_Row_Cache cachebufs2[L2_NR_ROWS][L2_WAYS_SIZE];

int in_L1_cache(hwaddr_t addr) {
	L1_cache_addr temp;
	temp.addr = addr;
	int set = temp.set;
	int mark = temp.mark;

	int i;
	for (i = 0; i < L1_WAYS_SIZE; i++)
		if (cachebufs[set][i].valid && cachebufs[set][i].mark == mark)
			return i;
	return -1;
}

#ifdef ENABLE_L2_CACHE
int in_L2_cache(hwaddr_t addr) {
	L2_cache_addr temp2;
	temp2.addr = addr;
	int set = temp2.set;
	int mark = temp2.mark;
	
	int i;
	for (i = 0; i < L2_WAYS_SIZE; i++)
		if (cachebufs2[set][i].valid && cachebufs2[set][i].mark == mark)
			return i;
	return -1;
}
#endif

void read_L1_cache(hwaddr_t addr, void *data) {
	L1_cache_addr temp;
	temp.addr = addr & ~(BLOCK_SIZE - 1);
	int set = temp.set;
	int mark = temp.mark;
	
#ifdef ENABLE_L2_CACHE
	L2_cache_addr temp2;
	temp2.addr = temp.addr;
	int set2 = temp2.set;
	int mark2 = temp2.mark;
	int idx2;
#endif

	int idx = in_L1_cache(addr);
	int i;
	
	if (idx < 0) {
		// TODO: here can be largely optimized
#ifdef ENABLE_L2_CACHE
		idx2 = in_L2_cache(addr);
		if (idx2 < 0) {
			idx2 = rand() & (L2_WAYS_SIZE - 1);
			/* handle dirty bit */
			if (cachebufs2[set2][idx2].valid && cachebufs2[set2][idx2].dirty) {
				L2_cache_addr temp3 = temp2;
				temp3.mark = cachebufs2[set2][idx2].mark;
				hwaddr_t new_addr = temp3.addr;
				for (i = 0; i < BLOCK_SIZE; i += 4)
					my_dram_write(new_addr + i, 4, *(uint32_t *)(cachebufs2[set2][idx2].buf + i));
			}
		
		
			for (i = 0; i < BLOCK_SIZE; i += 4) {
				*(uint32_t *)(cachebufs2[set2][idx2].buf + i) = my_dram_read(temp2.addr + i, 4);
			}
			cachebufs2[set2][idx2].mark = mark2;
			cachebufs2[set2][idx2].valid = true;
			cachebufs2[set2][idx2].dirty = false;
		}
		/* from L2 cache to L1 cache */
		idx = rand() & (L1_WAYS_SIZE - 1);
		
		memcpy(cachebufs[set][idx].buf, cachebufs2[set2][idx2].buf, BLOCK_SIZE);
		cachebufs[set][idx].mark = mark;
		cachebufs[set][idx].valid = true;
#else
		idx = rand() & (L1_WAYS_SIZE - 1);
		for (i = 0; i < BLOCK_SIZE; i += 4) {
			*(uint32_t *)(cachebufs[set][idx].buf + i) = my_dram_read(temp.addr + i, 4);
		}
		cachebufs[set][idx].mark = mark;
		cachebufs[set][idx].valid = true;
#endif
	}

	memcpy(data, cachebufs[set][idx].buf, BLOCK_SIZE);
}


uint32_t cache_read(hwaddr_t addr, size_t len) {
	uint32_t offset = addr & (BLOCK_SIZE - 1);
	uint8_t temp[BLOCK_SIZE << 1];

	read_L1_cache(addr, temp);

	if (offset + len > BLOCK_SIZE) {
		read_L1_cache(addr + BLOCK_SIZE, temp + BLOCK_SIZE);
	}

	return unalign_rw(temp + offset, 4);
}

void write_L1L2_cache(hwaddr_t addr, void *data, uint8_t *mask) {
	L1_cache_addr temp;
	temp.addr = addr & ~(BLOCK_SIZE - 1);
	int set = temp.set;
	
#ifdef ENABLE_L2_CACHE
	int mark = temp.mark;
	
	L2_cache_addr temp2;
	temp2.addr = temp.addr;
	int set2 = temp2.set;
	int mark2 = temp2.mark;
	
	int idx2;
#endif
	int idx = in_L1_cache(addr);
	int i;
	if (idx < 0) {
#ifdef ENABLE_L2_CACHE
		idx2 = in_L2_cache(addr);
		if (idx2 < 0) {
			idx2 = rand() & (L2_WAYS_SIZE - 1);
			
			/* handle dirty bit */
			if (cachebufs2[set2][idx2].valid && cachebufs2[set2][idx2].dirty) {
				L2_cache_addr temp3 = temp2;
				temp3.mark = cachebufs2[set2][idx2].mark;
				hwaddr_t new_addr = temp3.addr;
				for (i = 0; i < BLOCK_SIZE; i += 4)
					my_dram_write(new_addr + i, 4, *(uint32_t *)(cachebufs2[set2][idx2].buf + i));
			}

			for (i = 0; i < BLOCK_SIZE; i += 4)
				*(uint32_t *)(cachebufs2[set2][idx2].buf + i) = my_dram_read(temp2.addr + i, 4);
			cachebufs2[set2][idx2].mark = mark2;
			cachebufs2[set2][idx2].valid = true;
			cachebufs2[set2][idx2].dirty = false;
		}
		
		/* write to L2 and then write to L1 */
		memcpy_with_mask(cachebufs2[set2][idx2].buf, data, BLOCK_SIZE, mask);
		cachebufs2[set2][idx2].dirty = true;
	
		idx = rand() & (L1_WAYS_SIZE - 1);
		memcpy(cachebufs[set][idx].buf, cachebufs2[set2][idx2].buf, BLOCK_SIZE);
		cachebufs[set][idx].mark = mark;
		cachebufs[set][idx].valid = true;
#else
		for (i = 0; i < BLOCK_SIZE; i++)
			if (mask[i])
				my_dram_write(temp.addr + i, 1, *(uint8_t *)(data + i));
#endif
	} else {
#ifdef ENABLE_L2_CACHE
		idx2 = in_L2_cache(addr);
		memcpy_with_mask(cachebufs[set][idx].buf, data, BLOCK_SIZE, mask);
		
		memcpy(cachebufs2[set2][idx2].buf, cachebufs[set][idx].buf, BLOCK_SIZE);
		cachebufs2[set2][idx2].dirty = true;
		
		for (i = 0; i < BLOCK_SIZE; i += 4)
				my_dram_write(temp.addr + i, 4, *(uint32_t *)(cachebufs[set][idx].buf + i));
#else
		memcpy_with_mask(cachebufs[set][idx].buf, data, BLOCK_SIZE, mask);

		for (i = 0; i < BLOCK_SIZE; i += 4)
			my_dram_write(temp.addr + i, 4, *(uint32_t *)(cachebufs[set][idx].buf + i));
#endif
	}
}


void cache_write(hwaddr_t addr, size_t len, uint32_t data) {
	uint32_t offset = addr & (BLOCK_SIZE - 1);
	uint8_t temp[BLOCK_SIZE << 1];
	uint8_t mask[BLOCK_SIZE << 1];
	memset(mask, 0, 2 * BLOCK_SIZE);

	*(uint32_t *)(temp + offset) = data;
	memset(mask + offset, 1, len);
	write_L1L2_cache(addr, temp, mask);
	
	if (offset + len > BLOCK_SIZE)
		write_L1L2_cache(addr + BLOCK_SIZE, temp + BLOCK_SIZE, mask + BLOCK_SIZE);
}


void init_cache() {
	int i, j;
	for (i = 0; i < L1_NR_ROWS; i++)
		for (j = 0; j < L1_WAYS_SIZE; j++)
			cachebufs[i][j].valid = false;
	
	for (i = 0; i < L2_NR_ROWS; i++)
		for (j = 0; j < L2_WAYS_SIZE; j++)
			cachebufs2[i][j].valid = false;	
};
