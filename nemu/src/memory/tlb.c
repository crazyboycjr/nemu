#include "memory/tlb.h"
#include <stdlib.h>

TLB_item tlbbufs[TLB_SIZE];

int in_TLB(lnaddr_t addr) {
	uint32_t tag = addr >> 12;
	int i;
	/* TODO: this find procedure can be hash and optimizer */

	for (i = 0; i < TLB_SIZE; i++)
		if (tlbbufs[i].valid && tlbbufs[i].tag == tag)
			return i;
	return -1;
}

void substitude_TLB(uint32_t tag, uint32_t page_frame) {
	int idx = rand() & (TLB_SIZE - 1);
	tlbbufs[idx].tag = tag;
	tlbbufs[idx].page_frame = page_frame;
	tlbbufs[idx].valid = true;
}

void init_tlb() {
	int i;
	for (i = 0; i < TLB_SIZE; i++)
		tlbbufs[i].valid = false;
}

