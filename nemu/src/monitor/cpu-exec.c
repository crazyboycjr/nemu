#include "monitor/monitor.h"
#include "monitor/watchpoint.h"
#include "monitor/expr.h"
#include <setjmp.h>
#include "cpu/helper.h"
#include "device/i8259.h"

/* The assembly code of instructions executed is only output to the screen
 * when the number of instructions executed is less than this value.
 * This is useful when you use the ``si'' command.
 * You can modify this value as you want.
 */
#define MAX_INSTR_TO_PRINT 1000

int nemu_state = STOP;

int exec(swaddr_t);

char assembly[80];
char asm_buf[128];

/* Used with exception handling. */
jmp_buf jbuf;

void print_bin_instr(swaddr_t eip, int len) {
	int i;
	int l = sprintf(asm_buf, "%8x:   ", eip);
	for(i = 0; i < len; i ++) {
		l += sprintf(asm_buf + l, "%02x ", instr_fetch(eip + i, 1));
	}
	sprintf(asm_buf + l, "%*.s", 50 - (12 + 3 * len), "");
}

/* This function will be called when an `int3' instruction is being executed. */
void do_int3() {
	printf("\nHit breakpoint at eip = 0x%08x\n", cpu.eip);
	nemu_state = STOP;
}

extern WP *head;

/* Simulate how the CPU works. */
void cpu_exec(volatile uint32_t n) {
	if(nemu_state == END) {
		printf("Program execution has ended. To restart the program, exit NEMU and run again.\n");
		return;
	}
	nemu_state = RUNNING;

#ifdef DEBUG
	volatile unsigned long long n_temp = n;
#endif

	setjmp(jbuf);

	int j;
	for(j = 0; j < n; j ++) {
#ifdef DEBUG
		swaddr_t eip_temp = cpu.eip;
		if((n & 0xffff) == 0) {
			/* Output some dots while executing the program. */
			fputc('.', stderr);
		}
#endif

		/* Execute one instruction, including instruction fetch,
		 * instruction decode, and the actual execution. */
		int instr_len = exec(cpu.eip);

		cpu.eip += instr_len;

#ifdef DEBUG
		print_bin_instr(eip_temp, instr_len);
		strcat(asm_buf, assembly);
		Log_write("%s\n", asm_buf);
		if(n_temp < MAX_INSTR_TO_PRINT) {
			printf("%s\n", asm_buf);
		}
#endif

		/* Check watchpoints here. */
		WP		*ptr;
		bool	success;
		uint32_t	val;
		for (ptr = head; NULL != ptr; ptr = ptr->next) { 
			if (ptr->is_breakpoint) {
				val = expr(ptr->expr+1, &success);
				// TODO: why not use cpu.eip?
				uint32_t val2 = expr("$eip", &success);
				if (val2 == val) {
					printf("Hit breakpoint at 0x%.8x\n", val2);
					nemu_state = STOP;
				}
			}
			else if ((val = expr(ptr->expr, &success)) != ptr->cur_val && success) {
				printf("$eip = 0x%.8x\n", cpu.eip);
				printf("Watchpoint %d: %s\n\n", ptr->NO, ptr->expr);
				printf("Old value = %d\n", ptr->cur_val);
				printf("New value = %d\n", val);
				ptr->cur_val = val;
				nemu_state = STOP;
			}
		}

		if(nemu_state != RUNNING) { return; }
		
		if(cpu.INTR & cpu.eflags.IF) {
			uint32_t intr_no = i8259_query_intr();
			i8259_ack_intr();
			raise_intr(intr_no);
		}
	}

	if(nemu_state == RUNNING) { nemu_state = STOP; }
}
