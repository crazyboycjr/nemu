#include "nemu.h"

/* We use the POSIX regex functions to process regular expressions.
 * Type 'man regex' for more information about POSIX regex functions.
 */
#include <elf.h>
#include <sys/types.h>
#include <regex.h>
#include <stdlib.h>

enum {
	NOTYPE = 256, EQ, NE, AND, OR, SHL, SHR,
	LE, GE, NOT, DEREF, NEG, INV,
	LEF, RIG, REG, VAR, NUM
	/* TODO: Add more token types */

};

static struct rule {
	char *regex;
	int token_type;
} rules[] = {

	/* TODO: Add more rules.
	 * Pay attention to the precedence level of different rules.
	 */

	{" +",	NOTYPE},				// spaces
	{"\\+", '+'},					// plus
	{"\\-", '-'},
	{"\\*", '*'},
	{"/", '/'},
	{"%", '%'},
	{"<<", SHL},
	{">>", SHR},
	{"<=", LE},
	{">=", GE},
	{"==", EQ},						// equal
	{"!=", NE},
	{"&&", AND},
	{"\\|\\|", OR},
	{"!", NOT},
	{"~", INV},
	{"<", '<'},
	{">", '>'},
	{"&", '&'},
	{"\\^", '^'},
	{"\\|", '|'},
	{"\\(", LEF},
	{"\\)", RIG},
	{"\\$", REG},
	{"[a-zA-Z_][a-zA-Z_0-9]*", VAR},
	{"(0x)[0-9a-fA-F]+|[0-9]+", NUM}
};

#define NR_REGEX (sizeof(rules) / sizeof(rules[0]) )

static regex_t re[NR_REGEX];
static int pri[1024];

Elf32_Addr get_addr(char *, bool *);

/* Rules are used for many times.
 * Therefore we compile them only once before any usage.
 */

void init_pri() {
	/* TODO clac the priority level of operator */
	pri[NOT] = pri[INV] = pri[NEG] = pri[DEREF] = 2;
	pri['*'] = pri['/'] = pri['%'] = 3;
	pri['+'] = pri['-'] = 4;
	pri[SHL] = pri[SHR] = 5;
	pri['<'] = pri[LE] = pri['>'] = pri[GE] = 6;
	pri[EQ] = pri[NE] = 7;
	pri['&'] = 8;
	pri['^'] = 9;
	pri['|'] = 10;
	pri[AND] = 11;
	pri[OR] = 12;
}

void init_regex() {
	int i;
	char error_msg[128];
	int ret;

	init_pri();

	for(i = 0; i < NR_REGEX; i ++) {
		ret = regcomp(&re[i], rules[i].regex, REG_EXTENDED);
		if(ret != 0) {
			regerror(ret, &re[i], error_msg, 128);
			Assert(ret != 0, "regex compilation failed: %s\n%s", error_msg, rules[i].regex);
		}
	}
}

typedef struct token {
	int type;
	char str[32];
} Token;

Token tokens[2048];
int nr_token;

static bool make_token(char *e) {
	int position = 0;
	int i;
	regmatch_t pmatch;
	
	nr_token = 0;

	while(e[position] != '\0') {
		/* Try all rules one by one. */
		for(i = 0; i < NR_REGEX; i ++) {
			if(regexec(&re[i], e + position, 1, &pmatch, 0) == 0 && pmatch.rm_so == 0) {
				char *substr_start = e + position;
				int substr_len = pmatch.rm_eo;

				//Log("match rules[%d] = \"%s\" at position %d with len %d: %.*s", i, rules[i].regex, position, substr_len, substr_len, substr_start);
				position += substr_len;

				/* Now a new token is recognized with rules[i]. Add codes
				 * to record the token in the array ``tokens''. For certain 
				 * types of tokens, some extra actions should be performed.
				 */
				tokens[nr_token].type = rules[i].token_type;

				switch(rules[i].token_type) {
					case NUM: 
						strncpy(tokens[nr_token].str, substr_start, substr_len);
						tokens[nr_token].str[substr_len] = '\0';
						break;
					case REG:
						strncpy(tokens[nr_token].str, substr_start+1, 3);
						tokens[nr_token].str[3] = '\0';
						position += 3;
						break;
					case VAR:
						strncpy(tokens[nr_token].str, substr_start, substr_len);
						tokens[nr_token].str[substr_len] = '\0';
						break;
					//default: panic("please implement me");
				}
				nr_token++;

				break;
			}
		}

		if(i == NR_REGEX) {
			printf("no match at position %d\n%s\n%*.s^\n", position, e, position, "");
			return false;
		}
	}

	return true; 
}

static int stack[1024];

int check_parentheses(int l, int r) {
	/*
	 * 0 ok 1 not a pair 2 error
	 */
	int i, top = 0;
	stack[1] = 0;
	for (i = l; i <= r; i++) {
		if (tokens[i].type == LEF) {
			stack[++top] = i;
		}
		if (tokens[i].type == RIG) {
			top--;
		}
		if (top < 0) return 2;
	}
	return !(top==0 && (tokens[l].type==LEF && tokens[r].type==RIG
				&& stack[1] == l));
}

int dominant_op(int l, int r) {
	int top = 0;
	int i, max = 0, ret = 0, type;

	for (i = l; i <= r; i++) {
		type = tokens[i].type;
		if (type == LEF) top++;
		if (type == RIG) top--;

		if (top == 0 && ( (max != 2 && max <= pri[type])
					|| (max == 2 && max < pri[type]) )) {
			max = pri[type];
			ret = i;
		}
	}
	return ret;
}

uint32_t eval(int l, int r, bool *success) {
	if (l < r) {
		if (tokens[l].type == NOTYPE) ++l;
		if (tokens[r].type == NOTYPE) --r;
	}
	if (l > r) {
		*success = false;
		return 0;
	} else if (l == r) {
		uint32_t ret;
		int 	i;
		if (tokens[l].type == NUM)
			ret = strtol(tokens[l].str, NULL, 0);
		else if (tokens[l].type == REG) {
			if (strcmp("INT", tokens[l].str) == 0) {
				*success = true;
				return cpu.INTR;
			}
			/* control register */
			if (strcmp("cr0", tokens[l].str) == 0) {
				*success = true;
				return cpu.cr0.val;
			}
			if (strcmp("cr3", tokens[l].str) == 0) {
				*success = true;
				return cpu.cr3.val;
			}
			/* segment register */
			for (i = R_ES; i <= R_DS; i++)
				if (strcmp(sregs[i], tokens[l].str) == 0)
					break;
			if (i <= R_DS) {
				*success = true;
				return cpu.segr[i].val;
			}
			/* general propose register */
			for (i = R_EAX; i <= R_EDI; i++)
				if (strcmp(regsl[i], tokens[l].str) == 0)
					break;
			if (strcmp("eip", tokens[l].str) == 0)
				ret = cpu.eip;
			else {
				if (i > R_EDI) {
					*success = false;
					return 0;
				}
				ret = reg_l(i);
			}
		} else if (tokens[l].type == VAR) {
			ret = get_addr(tokens[l].str, success);
		} else {
			ret = 0;
			*success = false;
		}
		return ret;
	} else {
		int tmp = check_parentheses(l, r);
		if (tmp == 2) {
			*success = false;
			return 0;
		} else if (tmp == 0) {
			return eval(l+1, r-1, success);
		}
		int op = dominant_op(l, r);
		int type = tokens[op].type;
		uint32_t val1, val2;
		if (type == NEG || type == DEREF || type == NOT || type == INV) {
			val2 = eval(op+1, r, success);
			switch (type) {
				case NEG: return -val2;
				case NOT: return !val2;
				case INV: return ~val2;
				case DEREF: return swaddr_read(val2, 4, R_DS);
				default: *success = false; return 0;
			}
		}
		val1 = eval(l, op-1, success);
		val2 = eval(op+1, r, success);
		switch (tokens[op].type) {
			case '+': return val1 + val2;
			case '-': return val1 - val2;
			case '*': return val1 * val2;
			case '/': return val1 / val2;
			case '%': return val1 % val2;
			case SHL: return val1 << val2;
			case SHR: return val1 >> val2;
			case LE : return val1 <= val2;
			case GE : return val1 >= val2;
			case EQ : return val1 == val2;
			case NE : return val1 != val2;
			case AND: return val1 && val2;
			case OR : return val1 || val2;
			case '<': return val1 < val2;
			case '>': return val1 > val2;
			case '&': return val1 & val2;
			case '^': return val1 ^ val2;
			case '|': return val1 | val2;
			default: *success = false; return 0;
		}
	}
}

bool is_single_op(uint32_t type, int pos,int last) {
	return tokens[pos].type == type
		&& (pos == 0 || pri[tokens[last].type] > 0
				|| tokens[last].type == LEF);
}

uint32_t expr(char *e, bool *success) {
	if(!make_token(e)) {
		*success = false;
		return 0;
	}

	int i, last = -1;
	for (i = 0; i < nr_token; i++) {
		if (is_single_op('*', i, last))
			tokens[i].type = DEREF;
		if (is_single_op('-', i, last))
			tokens[i].type = NEG;
		if (tokens[i].type != NOTYPE)
			last = i;
	}

	*success = true;
	uint32_t	ret;
	ret = eval(0, nr_token-1, success);
	return ret;
}

