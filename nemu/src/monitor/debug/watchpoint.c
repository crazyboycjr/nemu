#include "monitor/watchpoint.h"
#include "monitor/expr.h"

#define NR_WP 1024 

static WP wp_list[NR_WP];
WP *head, *free_;

void init_wp_list() {
	int i;
	for(i = 0; i < NR_WP; i ++) {
		wp_list[i].NO = i;
		wp_list[i].is_breakpoint = false;
		memset(wp_list[i].expr, 0, sizeof wp_list[i].expr);
		wp_list[i].next = &wp_list[i + 1];
	}
	wp_list[NR_WP - 1].next = NULL;

	head = NULL;
	free_ = wp_list;
}

WP* new_wp() {
    if (free_ == NULL) {
        assert(0);
    }
    WP *wp = free_;
    free_ = free_->next;
	wp->next = head;
	head = wp;
    return wp;
}

void free_wp(WP *wp) {
    WP *p = free_;
    if (!p) assert(0);
    while (p != NULL) {
        if (p->next == NULL) {
			memset(wp->expr, 0, sizeof wp->expr);
            wp->next = NULL;
            wp->NO = p->NO + 1;
			wp->is_breakpoint = false;
            p->next = wp;
			break;
        }
		p = p->next;
    }
}
