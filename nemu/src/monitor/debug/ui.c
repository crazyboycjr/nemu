#include "monitor/monitor.h"
#include "monitor/expr.h"
#include "monitor/watchpoint.h"
#include "nemu.h"
#include "memory/cache.h"

#include <elf.h>
#include <stdlib.h>
#include <limits.h>
#include <readline/readline.h>
#include <readline/history.h>

void cpu_exec(uint32_t);
WP* new_wp();
void free_wp(WP*);
extern WP *head;
extern L1_Row_Cache cachebufs[L1_NR_ROWS][L1_WAYS_SIZE];
extern L2_Row_Cache cachebufs2[L2_NR_ROWS][L2_WAYS_SIZE];

char *get_func_name(swaddr_t);

/* We use the ``readline'' library to provide more flexibility to read from stdin. */
char* rl_gets() {
	static char *line_read = NULL;

	if (line_read) {
		free(line_read);
		line_read = NULL;
	}

	line_read = readline("(nemu) ");

	if (line_read && *line_read) {
		add_history(line_read);
	}

	return line_read;
}

static int cmd_c(char *args) {
	cpu_exec(-1);
	return 0;
}

static int cmd_q(char *args) {
	return -1;
}

static int cmd_help(char *args);
static int cmd_si(char *args);
static int cmd_info(char *args);
static int cmd_x(char *args);
static int cmd_p(char *args);
static int cmd_pp(char *args);
static int cmd_w(char *args);
static int cmd_d(char *args);
static int cmd_b(char *args);
static int cmd_bt(char *args);
static int cmd_cache(char *args);

static struct {
	char *name;
	char *description;
	int (*handler) (char *);
} cmd_table [] = {
	{ "help", "Display informations about all supported commands", cmd_help },
	{ "c", "Continue the execution of the program", cmd_c },
	{ "q", "Exit NEMU", cmd_q },
	{ "si", "Argument N means step N times", cmd_si },
	{ "info", "Display status of the program", cmd_info },
	{ "x", "Display N 4-bytes at the address", cmd_x},
	{ "p", "Calculate the value of the expression", cmd_p},
	{ "pp", "Calculate the value of the expression", cmd_pp},
	{ "w", "Set watchpoint", cmd_w},
	{ "d", "Delete watchpoint", cmd_d},
	{ "b", "Set breakpoint", cmd_b},
	{ "bt", "Backtrace", cmd_bt},
	{ "cache", "Test if addr is in cache", cmd_cache}
	/* TODO: Add more commands */

};

#define NR_CMD (sizeof(cmd_table) / sizeof(cmd_table[0]))

static int cmd_help(char *args) {
	/* extract the first argument */
	char *arg = strtok(NULL, " ");
	int i;

	if(arg == NULL) {
		/* no argument given */
		for(i = 0; i < NR_CMD; i ++) {
			printf("%s - %s\n", cmd_table[i].name, cmd_table[i].description);
		}
	}
	else {
		for(i = 0; i < NR_CMD; i ++) {
			if(strcmp(arg, cmd_table[i].name) == 0) {
				printf("%s - %s\n", cmd_table[i].name, cmd_table[i].description);
				return 0;
			}
		}
		printf("Unknown command '%s'\n", arg);
	}
	return 0;
}

static int cmd_si(char *args) {
	char *arg = strtok(NULL, " ");
	int n;

	if (NULL == arg) {
		n = 1;
	} else {
		n = strtol(arg, NULL, 0);
		//Assert(LONG_MIN <= n && n < LONG_MAX, "An overflow occurs.\n");
		if (n < 0) {
			printf("invalid operation.");
			return 0;
		}
	}
	cpu_exec(n);

	return 0;
}

static int cmd_info(char *args) {
	char *arg = strtok(NULL, " ");

	if (NULL == arg) {
		printf("No argument given.\n");
	} else {
		if (strcmp(arg, "r") == 0) {
			/* print register info */
			int i;
			for (i = R_EAX; i <= R_EDI; i++) {
				printf("%s\t0x%x\t%d\n", regsl[i], reg_l(i), reg_l(i));
			}
			printf("eip\t0x%x\t%d\n", cpu.eip, cpu.eip);
		} else if (strcmp(arg, "w") == 0) {
			/* print watch point info */
			WP	*ptr;
			for (ptr = head; NULL != ptr; ptr = ptr->next) {
				if (ptr->is_breakpoint) {
					printf("Breakpoint %d:\t%s\n",
							ptr->NO, ptr->expr);
				} else {
					printf("Watchpoint %d:\t%s\t%d\n",
							ptr->NO, ptr->expr, ptr->cur_val);
				}
			}
		} else {
			printf("Unknown argument '%s'\n", arg);
		}
	}

	return 0;
}

static int cmd_x(char *args) {
	char	*args_end = args + strlen(args);
	char	*arg = strtok(NULL, " ");
	char	*expr_arg;
	int		n, i;
	
	if (NULL == arg) {
		printf("No argument given.\n");
		printf("please input 'help x' to see more details.\n");
	} else {
		n = strtol(arg, NULL, 0);
		expr_arg = arg + strlen(arg) + 1;
		if (expr_arg >= args_end) {
			printf("Two arguments should be given.\n");
			return 0;
		}
		bool		success;
		uint32_t	st;
		st = expr(expr_arg, &success);
		if (!success) {
			printf("invalid expression\n");
			return 0;
		}
		for (i = 0; i < n<<2; i+=4) {
			if (i % 16 == 0)
				printf("0x%.8x:\t",st + i);
			printf(" 0x%.8x",swaddr_read(st + i, 4, R_DS));
			if (i % 16 == 12) printf("\n");
		}
		printf("\n");
	}
	return 0;
}

static int cmd_p(char *args) {
	bool		success;
	uint32_t	val = 0;

	if (NULL == args) {
		printf("No argument given.\n");
	} else {
		success = true;
		val = expr(args, &success);
		if (success)
			printf("%d\n", val);
		else
			printf("Please check the validity of your expression.\n");
	}
	return 0;
}

static int cmd_pp(char *args) {
	bool		success;
	uint32_t	val = 0;

	if (NULL == args) {
		printf("No argument given.\n");
	} else {
		success = true;
		val = expr(args, &success);
		if (success)
			printf("%x\n", val);
		else
			printf("Please check the validity of your expression.\n");
	}
	return 0;
}

static int cmd_w(char *args) {
	uint32_t val = 0;
	bool	success;

	if (NULL == args) {
		printf("No argument given.\n");
	} else {
		int	len = strlen(args);
		WP *wp = new_wp();
		success = true;
		val = expr(args, &success);
		if (success) {
			strncpy(wp->expr, args, len);
			*(wp->expr+len) = '\0';
			wp->cur_val = val;
			printf("Watchpoint %d ,now equal to %d\n", wp->NO, val);
		} else {
			printf("Please check the validity of your expression.\n");
		}
	}
	return 0;
}

static int cmd_d(char *args) {
	char	*arg = strtok(NULL, " ");
	int 	n;

	if (NULL == arg) {
		printf("No argument given.\n");
	} else {
		n = strtol(arg, NULL, 0);
		WP *ptr = head, *last = NULL;
		while (ptr != NULL && ptr->NO != n) {
			last = ptr;
			ptr = ptr->next;
		}
		if (ptr != NULL) {
			if (last) {
				last->next = ptr->next;
			} else head = ptr->next;
			free_wp(ptr);
		}
	}
	return 0;
}

static int cmd_b(char *args) {
	uint32_t val = 0;
	bool	success;

	if (NULL == args) {
		printf("No argument given.\n");
	} else {
		int	len = strlen(args);
		WP *wp = new_wp();
		success = true;
		val = expr(args+1, &success);
		if (success) {
			strncpy(wp->expr, args, len);
			wp->expr[len] = '\0';
			wp->is_breakpoint = true;
			printf("Breakpoint %d at 0x%.8x.\n", wp->NO, val);
		} else {
			printf("Please check the validity of your expression.\n");
		}
	}
	return 0;
}

typedef struct {
    swaddr_t prev_ebp;
    swaddr_t ret_addr;
    uint32_t args[4];
} PartOfStackFrame;
PartOfStackFrame stackFrame;

static int cmd_bt(char *args) {

	if (NULL == args) {
		int num = 0;
		printf("#%d  %s\n", num++, get_func_name(cpu.eip));

		stackFrame.ret_addr = swaddr_read(cpu.ebp + 4, 4, R_SS);
		stackFrame.prev_ebp = swaddr_read(cpu.ebp, 4, R_SS);
		while (stackFrame.prev_ebp) {
			int i;
			for (i = 0; i < 4; i++)
				stackFrame.args[i] = swaddr_read(stackFrame.prev_ebp + 4 * i + 8, 4, R_SS);
			printf("#%d  0x%.8x in %s (", num++, stackFrame.ret_addr, get_func_name(stackFrame.ret_addr));
			for (i = 0; i < 3; i++)
				printf("%d, ", stackFrame.args[i]);
			printf("%d)\n", stackFrame.args[3]);
			
			stackFrame.ret_addr = swaddr_read(stackFrame.prev_ebp + 4, 4, R_SS);
			stackFrame.prev_ebp = swaddr_read(stackFrame.prev_ebp, 4, R_SS);
		}
	} else {
		printf("Extra arguments given.\n");
	}
	return 0;
}

static int cmd_cache(char *args) {
	uint32_t val = 0;
	bool	success;
	int		i;

	if (NULL == args) {
		printf("No argument given.\n");
	} else {
		success = true;
		val = expr(args, &success);
		if (success) {
			int idx = in_L1_cache(val);
			if (idx < 0) {
				printf("Address 0x%.8x not in the cache.\n", val);
			} else {
				if (idx < L1_WAYS_SIZE) {
					L1_cache_addr temp;
					temp.addr = val & ~(BLOCK_SIZE - 1);
					int set = temp.set;
					size_t nr = sizeof(cachebufs[set][idx].buf);
					printf("%d\n",idx);
					printf("mark = 0x%.8x\n",cachebufs[set][idx].mark);
					printf("%d\n",cachebufs[set][idx].valid);
					printf("Address 0x%.8x is in L1 cache.\n", val);
					for (i = 0; i < nr; i += 4) {
						if (i % 16 == 0)
							printf("0x%.8x:\t", temp.addr + i);
						printf(" 0x%.8x", *(uint32_t *)(cachebufs[set][idx].buf + i));
						if (i % 16 == 12) printf("\n");
					}
					printf("\n");
					
					idx = in_L2_cache(val);
					if (idx >= 0) {
						goto next;
					}
				} else {
					idx -= L1_WAYS_SIZE;
				next:;
					L2_cache_addr temp2;
					temp2.addr = val & ~(BLOCK_SIZE - 1);
					int set2 = temp2.set;
					size_t nr2 = sizeof(cachebufs2[set2][idx].buf);
					printf("%d\n",idx);
					printf("mark = 0x%.8x\n",cachebufs2[set2][idx].mark);
					printf("%d\n",cachebufs2[set2][idx].valid);
					printf("Address 0x%.8x is in L2 cache.\n", val);
					for (i = 0; i < nr2; i += 4) {
						if (i % 16 == 0)
							printf("0x%.8x:\t", temp2.addr + i);
						printf(" 0x%.8x", *(uint32_t *)(cachebufs2[set2][idx].buf + i));
						if (i % 16 == 12) printf("\n");
					}
					printf("\n");
				}
			}
		} else {
			printf("Please check the validity of your expression.\n");
		}
	}
	return 0;
}

void ui_mainloop() {
	while(1) {
		char *str = rl_gets();
		char *str_end = str + strlen(str);

		/* extract the first token as the command */
		char *cmd = strtok(str, " ");
		if(cmd == NULL) { continue; }

		/* treat the remaining string as the arguments,
		 * which may need further parsing
		 */
		char *args = cmd + strlen(cmd) + 1;
		if(args >= str_end) {
			args = NULL;
		}

#ifdef HAS_DEVICE
		extern void sdl_clear_event_queue(void);
		sdl_clear_event_queue();
#endif

		int i;
		for(i = 0; i < NR_CMD; i ++) {
			if(strcmp(cmd, cmd_table[i].name) == 0) {
				if(cmd_table[i].handler(args) < 0) { return; }
				break;
			}
		}

		if(i == NR_CMD) { printf("Unknown command '%s'\n", cmd); }
	}
}
