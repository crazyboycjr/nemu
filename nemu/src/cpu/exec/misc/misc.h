#ifndef __MISC_H__
#define __MISC_H__

make_helper(nop);
make_helper(int3);
make_helper(lea);
make_helper(intn);

make_helper(cld);
make_helper(std);
make_helper(clc);
make_helper(stc);
make_helper(cli);
make_helper(sti);

make_helper(lgdt);
make_helper(lidt);

make_helper(mov_cr2r);
make_helper(mov_r2cr);

make_helper(pusha);
make_helper(popa);

make_helper(iret);

make_helper(hlt);

#endif
