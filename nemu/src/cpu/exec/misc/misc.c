#include "cpu/exec/helper.h"
#include "cpu/decode/modrm.h"

make_helper(nop) {
	print_asm("nop");
	return 1;
}

make_helper(int3) {
	void do_int3();
	do_int3();
	print_asm("int3");

	return 1;
}

make_helper(intn) {
	uint8_t imm8 = instr_fetch(eip + 1, 1);
	
	cpu.eip += 2;
	print_asm("int $%u", imm8);
	raise_intr(imm8);
	assert(0);
	return 0;
}

make_helper(lea) {
	ModR_M m;
	m.val = instr_fetch(eip + 1, 1);
	int len = load_addr(eip + 1, &m, op_src);
	reg_l(m.reg) = op_src->addr;
	print_asm("leal %s,%%%s", op_src->str, regsl[m.reg]);
	return 1 + len;
}

make_helper(cld) {
	cpu.eflags.DF = 0;
	print_asm("cld");
	return 1;
}

make_helper(std) {
	cpu.eflags.DF = 1;
	print_asm("std");
	return 1;
}

make_helper(clc) {
	cpu.eflags.CF = 0;
	print_asm("clc");
	return 1;
}

make_helper(stc) {
	cpu.eflags.CF = 1;
	print_asm("stc");
	return 1;
}

make_helper(cli) {
	cpu.eflags.IF = 0;
	print_asm("cli");
	return 1;
}

make_helper(sti) {
	cpu.eflags.IF = 1;
	print_asm("sti");
	return 1;
}

make_helper(lgdt) {
	ModR_M m;
	m.val = instr_fetch(eip + 1, 1);
	int len = load_addr(eip + 1, &m, op_src);
	
	cpu.gdtr.limit = lnaddr_read(op_src->addr, 2);
	cpu.gdtr.base = lnaddr_read(op_src->addr + 2, 4);
	if (ops_decoded.is_data_size_16)
		cpu.gdtr.base &= 0x00FFFFFF;
	
	print_asm("lgdtl %s", op_src->str);
	return 1 + len;
}

make_helper(lidt) {
	ModR_M m;
	m.val = instr_fetch(eip + 1, 1);
	int len = load_addr(eip + 1, &m, op_src);
	
	cpu.idtr.limit = lnaddr_read(op_src->addr, 2);
	cpu.idtr.base = lnaddr_read(op_src->addr + 2, 4);
	if (ops_decoded.is_data_size_16)
		cpu.idtr.base &= 0x00FFFFFF;
	
	print_asm("lidtl %s", op_src->str);
	return 1 + len;
}

make_helper(mov_cr2r) {
	op_dest->size = 4;
	int len = read_ModR_M(eip + 1, op_dest, op_src);
	if (op_src->reg == 0) {
		reg_l(op_dest->reg) = cpu.cr0.val;
	} else if (op_src->reg == 3) {
		reg_l(op_dest->reg) = cpu.cr3.val;
	} else {
		panic("mov_cr2r: eip = %x should not get here.", cpu.eip);
	}
	print_asm("mov %%cr%d,%s", op_src->reg, op_dest->str);
	return 1 + len;
}

#include "memory/tlb.h"
make_helper(mov_r2cr) {
	op_src->size = 4;
	int len = read_ModR_M(eip + 1, op_src, op_dest);
	if (op_dest->reg == 0) {
		cpu.cr0.val = op_src->val;
	} else if (op_dest->reg == 3) {
		cpu.cr3.val = op_src->val;
#ifdef ENABLE_TLB
		init_tlb();
#endif
	} else {
		panic("mov_r2cr: eip = %x should not get here.", cpu.eip);
	}
	print_asm("mov %s,%%cr%d", op_src->str, op_dest->reg);
	return 1 + len;
}


inline static void push(uint32_t val, uint8_t size) {
	cpu.esp -= size;
	swaddr_write(cpu.esp, size, val, R_SS);
}

make_helper(pusha) {
	uint8_t size = (!ops_decoded.is_data_size_16 + 1) << 1;
	uint32_t temp = cpu.esp;
	push(cpu.eax, size);
	push(cpu.ecx, size);
	push(cpu.edx, size);
	push(cpu.ebx, size);
	push(temp, size);
	push(cpu.ebp, size);
	push(cpu.esi, size);
	push(cpu.edi, size);
	print_asm("pusha%c", ops_decoded.is_data_size_16 ? 'w' : 'l');
	return 1;
}

inline static void pop(uint32_t reg) {
	reg_l(reg) = swaddr_read(cpu.esp, 4, R_SS);
	cpu.esp += 4;
}

inline static void pop2(uint32_t reg) {
	reg_w(reg) = swaddr_read(cpu.esp, 2, R_SS);
	cpu.esp += 2;
}

make_helper(popa) {
	uint8_t size = (!ops_decoded.is_data_size_16 + 1) << 1;
	if (size == 4) {
		pop(R_EDI);
		pop(R_ESI);
		pop(R_EBP);
		cpu.esp += size;
		pop(R_EBX);
		pop(R_EDX);
		pop(R_ECX);
		pop(R_EAX);
	} else {
		printf("popa16??????\n");
		assert(0);
		pop2(R_EDI);
		pop2(R_ESI);
		pop2(R_EBP);
		cpu.esp += size;
		pop2(R_EBX);
		pop2(R_EDX);
		pop2(R_ECX);
		pop2(R_EAX);
	}
	print_asm("popa%c", ops_decoded.is_data_size_16 ? 'w' : 'l');
	return 1;
}

#include "x86-inc/mmu.h"
make_helper(iret) {
	if (!ops_decoded.is_data_size_16) {
		cpu.eip = swaddr_read(cpu.esp, 4, R_SS) - 1;
		cpu.esp += 4;
		cpu.cs.val = swaddr_read(cpu.esp, 4, R_SS);
		cpu.esp += 4;
		
		uint32_t si = cpu.cs.si << 3;

		uint32_t base = cpu.gdtr.base;
		SegDesc sd;
		sd.limit_15_0 = lnaddr_read(base + si, 2);
		sd.base_15_0 = lnaddr_read(base + si + 2, 2);
		sd.base_23_16 = lnaddr_read(base + si + 4, 1);
		sd.base_31_24 = lnaddr_read(base + si + 7, 1);
		sd.limit_19_16 = lnaddr_read(base + si + 6, 1) & 0xf;

		cpu.cs.cache.base = (((sd.base_31_24 << 8) + sd.base_23_16) << 8) + sd.base_15_0;
		cpu.cs.cache.limit = (sd.limit_19_16 << 4) + sd.limit_15_0;
		
		cpu.eflags.val = swaddr_read(cpu.esp, 4, R_SS);
		cpu.esp += 4;
	} else {
		printf("iret16????\n");
		assert(0);
	}
	print_asm("iret%c", ops_decoded.is_data_size_16 ? 'w' : 'l');
	return 1;
}

make_helper(hlt) {
	if (!(cpu.INTR)) cpu.eip--;
	print_asm("hlt");
	return 1;
}
