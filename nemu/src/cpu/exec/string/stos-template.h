#include "cpu/exec/template-start.h"

make_helper(concat(stos_, SUFFIX)) {
	
	MEM_W(cpu.edi, REG(R_EAX), R_ES);
	if (!cpu.eflags.DF) cpu.edi += DATA_BYTE;
	else cpu.edi -= DATA_BYTE;
	
	print_asm("stos");
	return 1;
}

#include "cpu/exec/template-end.h"
