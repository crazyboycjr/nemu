#include "cpu/exec/template-start.h"

make_helper(concat(cmps_, SUFFIX)) {
	
	DATA_TYPE edi = MEM_R(cpu.edi, R_ES);
	DATA_TYPE esi = MEM_R(cpu.esi, R_DS);
	DATA_TYPE result = esi - edi;
	
	cpu.eflags.OF = (MSB(esi) != MSB(edi))
		& (MSB(result) != MSB(esi));
	cpu.eflags.CF = esi < edi;
	cpu.eflags.SF = MSB(result);
	cpu.eflags.ZF = result == 0;
	cpu.eflags.PF = __builtin_popcount((result & 0xff) ^ 1) & 1;
	
	int delta = 0;
	if (!cpu.eflags.DF) delta += DATA_BYTE;
	else delta -= DATA_BYTE;
	cpu.esi += delta;
	cpu.edi += delta;

	print_asm("cmps");
	return 1;
}

#include "cpu/exec/template-end.h"
