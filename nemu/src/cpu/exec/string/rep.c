#include "cpu/exec/helper.h"

make_helper(exec);

make_helper(rep) {
	int len;
	int count = 0;
	if(instr_fetch(eip + 1, 1) == 0xc3) {
		/* repz ret */
		exec(eip + 1);
		len = 0;
	}
	else {
		while(cpu.ecx) {
			exec(eip + 1);
			count ++;
			cpu.ecx --;
			int opcode = ops_decoded.opcode;
			assert(opcode == 0xa4	// movsb
				|| opcode == 0xa5	// movsw
				|| opcode == 0xaa	// stosb
				|| opcode == 0xab	// stosw
				|| opcode == 0xa6	// cmpsb
				|| opcode == 0xa7	// cmpsw
				|| opcode == 0xae	// scasb
				|| opcode == 0xaf	// scasw
				);

			/* Jump out of the while loop if necessary. */
			if (opcode == 0xa6 || opcode == 0xa7 || opcode == 0xae || opcode == 0xaf) {
				if (!cpu.eflags.ZF) break;
			}
		}
		len = 1;
	}

#ifdef DEBUG
	char temp[80];
	sprintf(temp, "rep %s", assembly);
	sprintf(assembly, "%s[cnt = %d]", temp, count);
#endif
	
	return len + 1;
}

make_helper(repnz) {
	int len;
	int count = 0;
	if(instr_fetch(eip + 1, 1) == 0xc3) {
		/* repz ret */
		exec(eip + 1);
		len = 0;
	}
	else {
		while(cpu.ecx) {
			exec(eip + 1);
			count ++;
			cpu.ecx --;
			int opcode = ops_decoded.opcode;
			assert(opcode == 0xa4	// movsb
				|| opcode == 0xa5	// movsw
				|| opcode == 0xaa	// stosb
				|| opcode == 0xab	// stosw
				|| opcode == 0xa6	// cmpsb
				|| opcode == 0xa7	// cmpsw
				|| opcode == 0xae	// scasb
				|| opcode == 0xaf	// scasw
				);

			/* Jump out of the while loop if necessary. */
			if (opcode == 0xa6 || opcode == 0xa7 || opcode == 0xae || opcode == 0xaf) {
				if (cpu.eflags.ZF) break;
			}
		}
		len = 1;
	}

#ifdef DEBUG
	char temp[80];
	sprintf(temp, "repnz %s", assembly);
	sprintf(assembly, "%s[cnt = %d]", temp, count);
#endif
	
	return len + 1;
}
