#include "cpu/exec/template-start.h"

make_helper(concat(movs_, SUFFIX)) {

	MEM_W(cpu.edi, MEM_R(cpu.esi, R_DS), R_ES);
	int delta = 0;
	if (!cpu.eflags.DF) delta = DATA_BYTE;
	else delta = -DATA_BYTE;
	cpu.esi += delta;
	cpu.edi += delta;

	print_asm("movs");
	return 1;
}

#include "cpu/exec/template-end.h"
