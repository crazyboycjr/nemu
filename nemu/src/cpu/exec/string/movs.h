#ifndef __MOVS_H__
#define __MOVS_H__
uint32_t dram_read(hwaddr_t, size_t);
make_helper(movs_b);

make_helper(movs_v);

#endif
