#include "cpu/exec/template-start.h"

#define instr jmp
#define shift (32 - (DATA_BYTE << 3))
static void do_execute() {
	cpu.eip += (int)op_src->val << shift >> shift;
	if (ops_decoded.is_data_size_16) {
		cpu.eip &= 0x0000ffff;
	}
	print_asm_template1();
}
#undef shift

make_instr_helper(i)

#if DATA_BYTE == 2 || DATA_BYTE == 4
make_helper(concat(jmp_rm_, SUFFIX)) {
	int instr_len = concat(decode_rm_, SUFFIX)(eip + 1) + 1;
	cpu.eip = op_src->val - instr_len;
	if (ops_decoded.is_data_size_16) {
		cpu.eip &= 0x0000ffff;
	}
	print_asm_template1();
	return instr_len;
}
#endif

#include "cpu/exec/template-end.h"
