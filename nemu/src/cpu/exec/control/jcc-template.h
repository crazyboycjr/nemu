#include "cpu/exec/template-start.h"

#define shift (32 - (DATA_BYTE << 3))

#define instr jns
static void do_execute() {
	if (!cpu.eflags.SF)
		cpu.eip += (int)op_src->val << shift >> shift;
	if (ops_decoded.is_data_size_16) {
		cpu.eip &= 0x0000ffff;
	}
	print_asm_template1();
}
make_instr_helper(i)
#undef instr

#define instr je
static void do_execute() {
	if (cpu.eflags.ZF)
		cpu.eip += (int)op_src->val << shift >> shift;
	if (ops_decoded.is_data_size_16) {
		cpu.eip &= 0x0000ffff;
	}
	print_asm_template1();
}
make_instr_helper(i)
#undef instr

#define instr jge
static void do_execute() {
	if (cpu.eflags.SF == cpu.eflags.OF)
		cpu.eip += (int)op_src->val << shift >> shift;
	if (ops_decoded.is_data_size_16) {
		cpu.eip &= 0x0000ffff;
	}
	print_asm_template1();
}
make_instr_helper(i)
#undef instr

#define instr js
static void do_execute() {
	if (cpu.eflags.SF)
		cpu.eip += (int)op_src->val << shift >> shift;
	if (ops_decoded.is_data_size_16) {
		cpu.eip &= 0x0000ffff;
	}
	print_asm_template1();
}
make_instr_helper(i)
#undef instr

#define instr jne
static void do_execute() {
	if (!cpu.eflags.ZF)
		cpu.eip += (int)op_src->val << shift >> shift;
	if (ops_decoded.is_data_size_16) {
		cpu.eip &= 0x0000ffff;
	}
	print_asm_template1();
}
make_instr_helper(i)
#undef instr

#define instr jbe
static void do_execute() {
	if (cpu.eflags.CF || cpu.eflags.ZF)
		cpu.eip += (int)op_src->val << shift >> shift;
	if (ops_decoded.is_data_size_16) {
		cpu.eip &= 0x0000ffff;
	}
	print_asm_template1();
}
make_instr_helper(i)
#undef instr

#define instr ja
static void do_execute() {
	if (!cpu.eflags.CF && !cpu.eflags.ZF)
		cpu.eip += (int)op_src->val << shift >> shift;
	if (ops_decoded.is_data_size_16) {
		cpu.eip &= 0x0000ffff;
	}
	print_asm_template1();
}
make_instr_helper(i)
#undef instr

#define instr jae
static void do_execute() {
	if (!cpu.eflags.CF)
		cpu.eip += (int)op_src->val << shift >> shift;
	if (ops_decoded.is_data_size_16) {
		cpu.eip &= 0x0000ffff;
	}
	print_asm_template1();
}
make_instr_helper(i)
#undef instr

#define instr jl
static void do_execute() {
	if (cpu.eflags.SF != cpu.eflags.OF)
		cpu.eip += (int)op_src->val << shift >> shift;
	if (ops_decoded.is_data_size_16) {
		cpu.eip &= 0x0000ffff;
	}
	print_asm_template1();
}
make_instr_helper(i)
#undef instr

#define instr jle
static void do_execute() {
	if (cpu.eflags.ZF || cpu.eflags.SF != cpu.eflags.OF)
		cpu.eip += (int)op_src->val << shift >> shift;
	if (ops_decoded.is_data_size_16) {
		cpu.eip &= 0x0000ffff;
	}
	print_asm_template1();
}
make_instr_helper(i)
#undef instr

#define instr jg
static void do_execute() {
	if (!cpu.eflags.ZF && cpu.eflags.SF == cpu.eflags.OF)
		cpu.eip += (int)op_src->val << shift >> shift;
	if (ops_decoded.is_data_size_16) {
		cpu.eip &= 0x0000ffff;
	}
	print_asm_template1();
}
make_instr_helper(i)
#undef instr

#define instr jb
static void do_execute() {
	if (cpu.eflags.CF)
		cpu.eip += (int)op_src->val << shift >> shift;
	if (ops_decoded.is_data_size_16) {
		cpu.eip &= 0x0000ffff;
	}
	print_asm_template1();
}
make_instr_helper(i)
#undef instr

#define instr jo
static void do_execute() {
	if (cpu.eflags.OF)
		cpu.eip += (int)op_src->val << shift >> shift;
	if (ops_decoded.is_data_size_16) {
		cpu.eip &= 0x0000ffff;
	}
	print_asm_template1();
}
make_instr_helper(i)
#undef instr

#define instr jno
static void do_execute() {
	if (!cpu.eflags.OF)
		cpu.eip += (int)op_src->val << shift >> shift;
	if (ops_decoded.is_data_size_16) {
		cpu.eip &= 0x0000ffff;
	}
	print_asm_template1();
}
make_instr_helper(i)
#undef instr

#define instr jp
static void do_execute() {
	if (cpu.eflags.PF)
		cpu.eip += (int)op_src->val << shift >> shift;
	if (ops_decoded.is_data_size_16) {
		cpu.eip &= 0x0000ffff;
	}
	print_asm_template1();
}
make_instr_helper(i)
#undef instr

#define instr jnp
static void do_execute() {
	if (!cpu.eflags.PF)
		cpu.eip += (int)op_src->val << shift >> shift;
	if (ops_decoded.is_data_size_16) {
		cpu.eip &= 0x0000ffff;
	}
	print_asm_template1();
}
make_instr_helper(i)
#undef instr

#undef shift

#include "cpu/exec/template-end.h"
