#include "cpu/exec/template-start.h"

#define instr ljmp
#include "x86-inc/mmu.h"
static void do_execute() {
	cpu.eip = op_src->val - DATA_BYTE - 3;
	if (ops_decoded.is_data_size_16) {
		cpu.eip &= 0x0000ffff;
	}
	/* write hidden part */
	
	cpu.cs.val = op_dest->val;
	uint16_t cs_val = cpu.cs.val >> 3 << 3;

	SegDesc temp;
	temp.limit_15_0 = lnaddr_read(cpu.gdtr.base + cs_val, 2);
	temp.base_15_0 = lnaddr_read(cpu.gdtr.base + cs_val + 2, 2);
	temp.base_23_16 = lnaddr_read(cpu.gdtr.base + cs_val + 4, 1);
	temp.base_31_24 = lnaddr_read(cpu.gdtr.base + cs_val + 7, 1);
	temp.limit_19_16 = lnaddr_read(cpu.gdtr.base + cs_val + 6, 1) & 0xf;
	
	cpu.cs.cache.base = (((temp.base_31_24 << 8) + temp.base_23_16) << 8) + temp.base_15_0;
	cpu.cs.cache.limit = (temp.limit_19_16 << 4) + temp.limit_15_0;
		
	print_asm_template2();
}

make_instr_helper(ptr)

#include "cpu/exec/template-end.h"
