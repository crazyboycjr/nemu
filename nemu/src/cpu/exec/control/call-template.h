#include "cpu/exec/template-start.h"

#define instr call

static void do_execute() {
	uint8_t size = (!ops_decoded.is_data_size_16 + 1) << 1;
	cpu.esp -= size;
	
	uint32_t temp_eip;
	
	if (ops_decoded.is_data_size_16) {
		assert(0);
		temp_eip = (cpu.eip + op_src->val) & 0x0000ffff;
		swaddr_write(cpu.esp, size, ((cpu.eip & 0x0000ffff)+ DATA_BYTE + 1) & 0x0000ffff, R_SS); //push()
		cpu.eip = temp_eip;
	} else {
			swaddr_write(cpu.esp, size, (cpu.eip + DATA_BYTE + 1), R_SS); //push()
			cpu.eip = cpu.eip + op_src->val;
	}
	print_asm_template1();
}

make_instr_helper(i)

#if DATA_BYTE == 2 || DATA_BYTE == 4
make_helper(concat(call_rm_, SUFFIX)) {
	int instr_len = concat(decode_rm_, SUFFIX)(eip + 1);
	uint8_t size = (!ops_decoded.is_data_size_16 + 1) << 1;
	cpu.esp -= size;
	swaddr_write(cpu.esp, size, (cpu.eip + instr_len + 1), R_SS);
	
	cpu.eip = op_src->val - instr_len - 1;
	if (ops_decoded.is_data_size_16) {
		assert(0);
		cpu.eip &= 0x0000ffff;
	}
	print_asm_template1();
	return instr_len + 1;
}
#endif


#include "cpu/exec/template-end.h"
