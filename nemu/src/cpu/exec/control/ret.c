#include "cpu/exec/helper.h"

make_helper(ret) {
	cpu.eip = swaddr_read(cpu.esp, 2 * (!ops_decoded.is_data_size_16 + 1), R_SS) - 1;
	cpu.esp += 2;
	if (!ops_decoded.is_data_size_16)
		cpu.esp += 2;
	else cpu.eip &= 0x0000ffff;
	print_asm("ret");
	return 1;
}

make_helper(ret_i_w) {
	int16_t imm = instr_fetch(eip, 2);
	cpu.eip = swaddr_read(cpu.esp, 2 * (!ops_decoded.is_data_size_16 + 1), R_SS) - 3;
	cpu.esp += 2;
	if (!ops_decoded.is_data_size_16)
		cpu.esp += 2;
	else cpu.eip &= 0x0000ffff;
	cpu.esp += imm;
	
	print_asm("ret");
	return 3;
}
