#include "cpu/exec/helper.h"

make_helper(leave) {
	cpu.esp = cpu.ebp;
	if (ops_decoded.is_data_size_16) {
		reg_w(R_EBP) = swaddr_read(cpu.esp, 4, R_SS);
		cpu.esp += 2;
	} else {
		cpu.ebp = swaddr_read(cpu.esp, 4, R_SS);
		cpu.esp += 4;
	}
	print_asm("leave");
	return 1;
}
