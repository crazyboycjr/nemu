#include "cpu/exec/helper.h"

make_helper(cltd) {
	if (!ops_decoded.is_data_size_16)
		cpu.edx = (int)cpu.eax >> 31;
	else reg_w(R_DX) = (short)(cpu.eax & 65535) >> 15;
	
	print_asm("cltd");
	return 1;
}
