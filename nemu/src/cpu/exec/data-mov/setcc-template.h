#include "cpu/exec/template-start.h"


#define instr setns
static void do_execute() {
	OPERAND_W(op_src, !cpu.eflags.SF);
	print_asm_template1();
}
make_instr_helper(rm)
#undef instr

#define instr sete
static void do_execute() {
	OPERAND_W(op_src, cpu.eflags.ZF);
	print_asm_template1();
}
make_instr_helper(rm)
#undef instr

#define instr setge
static void do_execute() {
	OPERAND_W(op_src, cpu.eflags.SF == cpu.eflags.OF);
	print_asm_template1();
}
make_instr_helper(rm)
#undef instr

#define instr sets
static void do_execute() {
	OPERAND_W(op_src, cpu.eflags.SF);
	print_asm_template1();
}
make_instr_helper(rm)
#undef instr

#define instr setne
static void do_execute() {
	OPERAND_W(op_src, !cpu.eflags.ZF);
	print_asm_template1();
}
make_instr_helper(rm)
#undef instr

#define instr setbe
static void do_execute() {
	OPERAND_W(op_src, cpu.eflags.CF || cpu.eflags.ZF);
	print_asm_template1();
}
make_instr_helper(rm)
#undef instr

#define instr seta
static void do_execute() {
	OPERAND_W(op_src, !cpu.eflags.CF && !cpu.eflags.ZF);
	print_asm_template1();
}
make_instr_helper(rm)
#undef instr

#define instr setae
static void do_execute() {
	OPERAND_W(op_src, !cpu.eflags.CF);
	print_asm_template1();
}
make_instr_helper(rm)
#undef instr

#define instr setl
static void do_execute() {
	OPERAND_W(op_src, cpu.eflags.SF != cpu.eflags.OF);
	print_asm_template1();
}
make_instr_helper(rm)
#undef instr

#define instr setle
static void do_execute() {
	OPERAND_W(op_src, cpu.eflags.ZF || cpu.eflags.SF != cpu.eflags.OF);
	print_asm_template1();
}
make_instr_helper(rm)
#undef instr

#define instr setg
static void do_execute() {
	OPERAND_W(op_src, !cpu.eflags.ZF && cpu.eflags.SF == cpu.eflags.OF);
	print_asm_template1();
}
make_instr_helper(rm)
#undef instr

#define instr setb
static void do_execute() {
	OPERAND_W(op_src, cpu.eflags.CF);
	print_asm_template1();
}
make_instr_helper(rm)
#undef instr

#define instr seto
static void do_execute() {
	OPERAND_W(op_src, cpu.eflags.OF);
	print_asm_template1();
}
make_instr_helper(rm)
#undef instr

#define instr setno
static void do_execute() {
	OPERAND_W(op_src, !cpu.eflags.OF);
	print_asm_template1();
}
make_instr_helper(rm)
#undef instr

#define instr setp
static void do_execute() {
	OPERAND_W(op_src, cpu.eflags.PF);
	print_asm_template1();
}
make_instr_helper(rm)
#undef instr

#define instr setnp
static void do_execute() {
	OPERAND_W(op_src, !cpu.eflags.PF);
	print_asm_template1();
}
make_instr_helper(rm)
#undef instr


#include "cpu/exec/template-end.h"
