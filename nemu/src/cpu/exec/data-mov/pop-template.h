#include "cpu/exec/template-start.h"

#define instr pop

static void do_execute() {
	uint8_t size = (!ops_decoded.is_data_size_16 + 1) << 1;
	OPERAND_W(op_src, swaddr_read(cpu.esp, size, R_SS));
	cpu.esp += size;
	print_asm_template1();
}

make_instr_helper(r)
make_instr_helper(rm)

#include "cpu/exec/template-end.h"
