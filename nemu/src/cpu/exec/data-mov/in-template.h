#include "cpu/exec/template-start.h"

#define instr in

static void do_execute () {
	REG(R_EAX) = pio_read(op_src->val, DATA_BYTE);
	
	print_asm(str(instr) str(SUFFIX) " %s,%%%s", op_src->str, REG_NAME(R_EAX));
}

make_instr_helper(imm8)

make_helper(concat(in_, SUFFIX)) {
	REG(R_EAX) = pio_read(cpu.edx & 0xffff, DATA_BYTE);
	print_asm(str(instr) str(SUFFIX) " (%%dx),%%%s", REG_NAME(R_EAX));
	return 1;
}

#include "cpu/exec/template-end.h"
