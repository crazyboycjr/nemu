#include "cpu/exec/template-start.h"

#define instr push

static void do_execute() {
	uint8_t size = (!ops_decoded.is_data_size_16 + 1) << 1;
	cpu.esp -= size;
	swaddr_write(cpu.esp, size, op_src->val, R_SS);
	print_asm_template1();
}

make_instr_helper(r)
make_instr_helper(rm)
make_instr_helper(i)

#include "cpu/exec/template-end.h"
