#include "cpu/exec/template-start.h"

#define instr out

static void do_execute () {
	pio_write(op_src->val, DATA_BYTE, REG(R_EAX));
	
	print_asm(str(instr) str(SUFFIX) " %%%s,%s", REG_NAME(R_EAX), op_src->str);
}

make_instr_helper(imm8)

make_helper(concat(out_, SUFFIX)) {
	pio_write(cpu.edx & 0xffff, DATA_BYTE, REG(R_EAX));
	print_asm(str(instr) str(SUFFIX) " %%%s,(%%dx)", REG_NAME(R_EAX));
	return 1;
}

#include "cpu/exec/template-end.h"
