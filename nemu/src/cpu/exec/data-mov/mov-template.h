#include "cpu/exec/template-start.h"

#define instr mov

static void do_execute() {
	OPERAND_W(op_dest, op_src->val);
	print_asm_template2();
}

make_instr_helper(i2r)
make_instr_helper(i2rm)
make_instr_helper(r2rm)
make_instr_helper(rm2r)

make_helper(concat(mov_a2moffs_, SUFFIX)) {
	swaddr_t addr = instr_fetch(eip + 1, 4);
	MEM_W(addr, REG(R_EAX), R_DS);

	print_asm("mov" str(SUFFIX) " %%%s,0x%x", REG_NAME(R_EAX), addr);
	return 5;
}

make_helper(concat(mov_moffs2a_, SUFFIX)) {
	swaddr_t addr = instr_fetch(eip + 1, 4);
	REG(R_EAX) = MEM_R(addr, R_DS);

	print_asm("mov" str(SUFFIX) " 0x%x,%%%s", addr, REG_NAME(R_EAX));
	return 5;
}

#if DATA_BYTE == 2
uint32_t lnaddr_read(swaddr_t, size_t);
#include "x86-inc/mmu.h"
make_helper(concat(mov_rm2sr_, SUFFIX)) {
	int len = decode_rm2sr_w(eip + 1);
	assert(op_dest->reg <= 3);
	cpu.segr[op_dest->reg].val = op_src->val;
	
	/* load hidden part */
	SegDesc temp;
	temp.limit_15_0 = lnaddr_read(cpu.gdtr.base + (op_src->val >> 3 << 3), 2);
	temp.base_15_0 = lnaddr_read(cpu.gdtr.base + (op_src->val >> 3 << 3) + 2, 2);
	temp.base_23_16 = lnaddr_read(cpu.gdtr.base + (op_src->val >> 3 << 3) + 4, 1);
	temp.base_31_24 = lnaddr_read(cpu.gdtr.base + (op_src->val >> 3 << 3) + 7, 1);
	temp.limit_19_16 = lnaddr_read(cpu.gdtr.base + (op_src->val >> 3 << 3) + 6, 1) & 0xf;
	
	cpu.segr[op_dest->reg].cache.base = (((temp.base_31_24 << 8) + temp.base_23_16) << 8) + temp.base_15_0;
	cpu.segr[op_dest->reg].cache.limit = (temp.limit_19_16 << 4) + temp.limit_15_0;
	
	print_asm("mov" str(SUFFIX) " %s,%%%s", op_src->str, sregs[op_dest->reg]);
	return len + 1;
}
#endif

#include "cpu/exec/template-end.h"
