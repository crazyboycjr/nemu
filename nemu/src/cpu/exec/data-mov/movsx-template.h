#include "cpu/exec/template-start.h"
#include "cpu/decode/modrm.h"

#define instr movsb

make_helper(concat(movsb_rm2r_, SUFFIX)) {
	op_src->size = 1;
	int len = read_ModR_M(eip + 1, op_src, op_dest);
	op_dest->val = REG(op_dest->reg);
	
	int shift = 24;
	int result = (int)op_src->val << shift >> shift;
	
	OPERAND_W(op_dest, result);
	
	print_asm_template2();
	return len + 1;
}

#if DATA_BYTE == 4
make_helper(concat(movsw_rm2r_, SUFFIX)) {
	op_src->size = 2;
	int len = read_ModR_M(eip + 1, op_src, op_dest);
	op_dest->val = REG(op_dest->reg);
	
	int shift = 16;
	int result = (int)op_src->val << shift >> shift;
	
	OPERAND_W(op_dest, result);
	
	print_asm(str(movsw) str(SUFFIX) " %s,%s", op_src->str, op_dest->str);
	return len + 1;
}
#endif

#include "cpu/exec/template-end.h"
