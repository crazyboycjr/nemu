#ifndef __OUT_H__
#define __OUT_H__

make_helper(out_b);
make_helper(out_imm8_b);

make_helper(out_v);
make_helper(out_imm8_v);

#endif
