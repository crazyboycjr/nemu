#ifndef __IN_H__
#define __IN_H__

make_helper(in_b);
make_helper(in_imm8_b);

make_helper(in_v);
make_helper(in_imm8_v);

#endif
