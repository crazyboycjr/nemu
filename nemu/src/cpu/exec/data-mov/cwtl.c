#include "cpu/exec/helper.h"

make_helper(cwtl) {
	if (!ops_decoded.is_data_size_16)
		reg_l(R_EAX) = (int)(cpu.eax & 65535) << 16 >> 16;
	else reg_w(R_AX) = (short)(cpu.eax & 255) << 8 >> 8;
	
	print_asm("cwtl");
	return 1;
}
