#include "cpu/exec/template-start.h"

#define instr cmovns
static void do_execute() {
	if (!cpu.eflags.SF)
		OPERAND_W(op_dest, op_src->val);
	print_asm_template2();
}
make_instr_helper(rm2r)
#undef instr

#define instr cmove
static void do_execute() {
	if (cpu.eflags.ZF)
		OPERAND_W(op_dest, op_src->val);
	print_asm_template2();
}
make_instr_helper(rm2r)
#undef instr

#define instr cmovge
static void do_execute() {
	if (cpu.eflags.SF == cpu.eflags.OF)
		OPERAND_W(op_dest, op_src->val);
	print_asm_template2();
}
make_instr_helper(rm2r)
#undef instr

#define instr cmovs
static void do_execute() {
	if (cpu.eflags.SF)
		OPERAND_W(op_dest, op_src->val);
	print_asm_template2();
}
make_instr_helper(rm2r)
#undef instr

#define instr cmovne
static void do_execute() {
	if (!cpu.eflags.ZF)
		OPERAND_W(op_dest, op_src->val);
	print_asm_template2();
}
make_instr_helper(rm2r)
#undef instr

#define instr cmovbe
static void do_execute() {
	if (cpu.eflags.CF || cpu.eflags.ZF)
		OPERAND_W(op_dest, op_src->val);
	print_asm_template2();
}
make_instr_helper(rm2r)
#undef instr

#define instr cmova
static void do_execute() {
	if (!cpu.eflags.CF && !cpu.eflags.ZF)
		OPERAND_W(op_dest, op_src->val);
	print_asm_template2();
}
make_instr_helper(rm2r)
#undef instr

#define instr cmovae
static void do_execute() {
	if (!cpu.eflags.CF)
		OPERAND_W(op_dest, op_src->val);
	print_asm_template2();
}
make_instr_helper(rm2r)
#undef instr

#define instr cmovl
static void do_execute() {
	if (cpu.eflags.SF != cpu.eflags.OF)
		OPERAND_W(op_dest, op_src->val);
	print_asm_template2();
}
make_instr_helper(rm2r)
#undef instr

#define instr cmovle
static void do_execute() {
	if (cpu.eflags.ZF || cpu.eflags.SF != cpu.eflags.OF)
		OPERAND_W(op_dest, op_src->val);
	print_asm_template2();
}
make_instr_helper(rm2r)
#undef instr

#define instr cmovg
static void do_execute() {
	if (!cpu.eflags.ZF && cpu.eflags.SF == cpu.eflags.OF)
		OPERAND_W(op_dest, op_src->val);
	print_asm_template2();
}
make_instr_helper(rm2r)
#undef instr

#define instr cmovb
static void do_execute() {
	if (cpu.eflags.CF)
		OPERAND_W(op_dest, op_src->val);
	print_asm_template2();
}
make_instr_helper(rm2r)
#undef instr

#define instr cmovo
static void do_execute() {
	if (cpu.eflags.OF)
		OPERAND_W(op_dest, op_src->val);
	print_asm_template2();
}
make_instr_helper(rm2r)
#undef instr

#define instr cmovno
static void do_execute() {
	if (!cpu.eflags.OF)
		OPERAND_W(op_dest, op_src->val);
	print_asm_template2();
}
make_instr_helper(rm2r)
#undef instr

#define instr cmovp
static void do_execute() {
	if (cpu.eflags.PF)
		OPERAND_W(op_dest, op_src->val);
	print_asm_template2();
}
make_instr_helper(rm2r)
#undef instr


#define instr cmovnp
static void do_execute() {
	if (!cpu.eflags.PF)
		OPERAND_W(op_dest, op_src->val);
	print_asm_template2();
}
make_instr_helper(rm2r)
#undef instr


#include "cpu/exec/template-end.h"
