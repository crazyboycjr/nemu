#include "cpu/exec/template-start.h"

#define instr adc

static void do_execute() {
	DATA_TYPE result2 = op_src->val + cpu.eflags.CF;
	DATA_TYPE result = op_dest->val + result2;

	cpu.eflags.OF = (MSB(op_dest->val) == MSB(result2)) & (MSB(result) != MSB(op_dest->val))
		|| (MSB(op_src->val) == MSB(cpu.eflags.CF)) & (MSB(result2) != MSB(op_src->val));
		
	cpu.eflags.CF = ((result < op_dest->val) && (result < result2))
		|| ((result2 < op_src->val) && (result2 < cpu.eflags.CF));

	cpu.eflags.SF = MSB(result);
	cpu.eflags.ZF = result == 0;
	cpu.eflags.PF = __builtin_popcount((result & 0xff) ^ 1) & 1;

	OPERAND_W(op_dest, result);

	print_asm_template2();
}

make_instr_helper(r2rm)
#if DATA_BYTE == 2 || DATA_BYTE == 4
make_instr_helper(si2rm)
#endif

make_instr_helper(i2rm)
make_instr_helper(rm2r)

#include "cpu/exec/template-end.h"
