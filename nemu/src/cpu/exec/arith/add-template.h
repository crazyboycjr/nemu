#include "cpu/exec/template-start.h"

#define instr add

static void do_execute() {
	DATA_TYPE result = op_dest->val + op_src->val;

	cpu.eflags.OF = (MSB(op_dest->val) == MSB(op_src->val))
		& (MSB(result) != MSB(op_dest->val));
	cpu.eflags.CF = (result < op_dest->val) && (result < op_src->val);
	cpu.eflags.SF = MSB(result);
	cpu.eflags.ZF = result == 0;
	cpu.eflags.PF = __builtin_popcount((result & 0xff) ^ 1) & 1;

	OPERAND_W(op_dest, result);

	print_asm_template2();
}

make_instr_helper(r2rm)
#if DATA_BYTE == 2 || DATA_BYTE == 4
make_instr_helper(si2rm)
#endif

make_instr_helper(i2rm)
make_instr_helper(rm2r)
make_instr_helper(i2a)

#include "cpu/exec/template-end.h"
