#include "cpu/exec/template-start.h"

#define instr dec

static void do_execute () {
	DATA_TYPE result = op_src->val - 1;
	OPERAND_W(op_src, result);

	cpu.eflags.OF = result == (1u << ((DATA_BYTE << 3) - 1)) - 1;
	cpu.eflags.CF = op_src->val == 0;
	cpu.eflags.SF = MSB(result);
	cpu.eflags.ZF = result == 0;
	cpu.eflags.PF = __builtin_popcount((result & 0xff) ^ 1) & 1;

	print_asm_template1();
}

make_instr_helper(rm)
#if DATA_BYTE == 2 || DATA_BYTE == 4
make_instr_helper(r)
#endif

#include "cpu/exec/template-end.h"
