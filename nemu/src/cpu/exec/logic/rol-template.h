#include "cpu/exec/template-start.h"

#define instr rol

static void do_execute () {
	DATA_TYPE src = op_src->val;
	DATA_TYPE dest = op_dest->val;

	uint8_t count = src & 0x1f, temp_count = count;
	while (temp_count--) {
		cpu.eflags.CF = MSB(dest);
		dest = (dest << 1) | cpu.eflags.CF;
	}
	cpu.eflags.CF = dest & 1;
	if (count == 1)
		cpu.eflags.OF = MSB(dest) ^ cpu.eflags.CF;
		
	OPERAND_W(op_dest, dest);

	print_asm_template2();
}

make_instr_helper(rm_imm)

#include "cpu/exec/template-end.h"
