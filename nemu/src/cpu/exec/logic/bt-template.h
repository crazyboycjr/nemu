#include "cpu/exec/template-start.h"

#define instr bt

static void do_execute () {
	DATA_TYPE src = op_src->val;
	DATA_TYPE dest = op_dest->val;

	uint8_t count = src & ((DATA_BYTE << 3) - 1);
	
	cpu.eflags.CF = dest >> count & 1;
	
	/* There is no need to update EFLAGS, since no other instructions 
	 * in PA will test the flags updated by this instruction.
	 */

	print_asm_template2();
}

make_instr_helper(r2rm)
make_instr_helper(rm_imm)

#include "cpu/exec/template-end.h"
