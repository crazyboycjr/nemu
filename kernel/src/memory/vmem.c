extern const unsigned char windows95_colors[];

#include "common.h"
#include "memory.h"
#include <string.h>

#define VMEM_ADDR 0xa0000
#define SCR_SIZE (320 * 200)

/* Use the function to get the start address of user page directory. */
__attribute__ ((gnu_inline)) inline PDE* get_updir();
#define UPTAB_SIZE (0xc0000 / PAGE_SIZE + 1)
static PTE uptable[UPTAB_SIZE] align_to_page;

void create_video_mapping() {
	/* Create an identical mapping from virtual memory area 
	 * [0xa0000, 0xa0000 + SCR_SIZE) to physical memory area 
	 * [0xa0000, 0xa0000 + SCR_SIZE) for user program. You may define
	 * some page tables to create this mapping.
	 */
	PDE *updir = get_updir();
	PTE *ptable = (PTE *)va_to_pa(uptable);

	updir[VMEM_ADDR / PT_SIZE].val = make_pde(ptable);
	memset(ptable, 0, sizeof(uptable));
	uint32_t pframe_addr = VMEM_ADDR;
	ptable += VMEM_ADDR / PAGE_SIZE;
	for (; pframe_addr < VMEM_ADDR + SCR_SIZE; pframe_addr += PAGE_SIZE) {
		ptable->val = make_pte(pframe_addr);
		ptable ++;
	}
}

void video_mapping_write_test() {
	int i;
	uint8_t *buf = (void *)VMEM_ADDR;
	for(i = 0; i < SCR_SIZE; i ++) {
		buf[i] = windows95_colors[i];
	}
}

void video_mapping_read_test() {
	int i;
	uint8_t *buf = (void *)VMEM_ADDR;
	for(i = 0; i < SCR_SIZE; i ++) {
		assert(buf[i] == windows95_colors[i]);
	}
}

void video_mapping_clear() {
	memset((void *)VMEM_ADDR, 0, SCR_SIZE);
}

