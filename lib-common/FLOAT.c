#include "FLOAT.h"

FLOAT F_mul_F(FLOAT a, FLOAT b) {
	FLOAT ret = 0, f = 1;
	if ((a >> 31) ^ (b >> 31)) f = -1;
	long long ta = Fabs(a);
	long long tb = Fabs(b);
	ret = ta * tb / 65536;
	return ret * f;
}

FLOAT F_div_F(FLOAT a, FLOAT b) {
	FLOAT ret = 0, f = 1;
	if ((a >> 31) ^ (b >> 31)) f = -1;
	a = Fabs(a);
	if (a == 0) return 0;
	b = Fabs(b);
	ret = a / b;
	a -= ret * b;
	int i = 0;
	for (i = 0; i < 16; i++) {
		a <<= 1;
		ret <<= 1;
		if (a >= b) {
			a -= b;
			ret++;
		}
	}
	return ret * f;
}

union {
	float a;
	int b;
} tran;

FLOAT f2F(float a) {
	tran.a = a;
	int tmp = tran.b;
	int y = tmp & (1 << 31);
	tmp = tmp & ((1u << 31) - 1);
	int shift = (((tmp & (0xff000000 >> 1)) >> 23) - 127) - 7;
	tmp = (tmp & (0x00ffffff >> 1)) | (1 << 23);
	if (shift >= 0) tmp <<= shift;
	else tmp >>= -shift;
	if (y) return -tmp;
	return tmp;
}

FLOAT Fabs(FLOAT a) {
	return a > 0 ? a : -a;
}

FLOAT sqrt(FLOAT x) {
	FLOAT dt, t = int2F(2);

	do {
		dt = F_div_int((F_div_F(x, t) - t), 2);
		t += dt;
	} while(Fabs(dt) > f2F(1e-4));

	return t;
}

FLOAT pow(FLOAT x, FLOAT y) {
	/* we only compute x^0.333 */
	FLOAT t2, dt, t = int2F(2);

	do {
		t2 = F_mul_F(t, t);
		dt = (F_div_F(x, t2) - t) / 3;
		t += dt;
	} while(Fabs(dt) > f2F(1e-4));

	return t;
}

