#include "hal.h"
#include "device/video.h"
#include "device/palette.h"

#include <string.h>
#include <stdlib.h>

int get_fps();

void SDL_BlitSurface(SDL_Surface *src, SDL_Rect *srcrect, 
		SDL_Surface *dst, SDL_Rect *dstrect) {
	assert(dst && src);

	/* TODO: Performs a fast blit from the source surface to the 
	 * destination surface. Only the position is used in the
	 * ``dstrect'' (the width and height are ignored). If either
	 * ``srcrect'' or ``dstrect'' are NULL, the entire surface 
	 * (``src'' or ``dst'') is copied. The final blit rectangle 
	 * is saved in ``dstrect'' after all clipping is performed
	 * (``srcrect'' is not modified).
	 */
	SDL_Rect fulldst;
	int srcx, srcy;
	int w, h;
	if (srcrect == NULL) {
		srcx = srcy = 0;
		w = src->w;
		h = src->h;
	} else {
		srcx = srcrect->x;
		srcy = srcrect->y;
		w = srcrect->w;
		h = srcrect->h;
	}
	
	if (dstrect == NULL) {
		fulldst.x = fulldst.y = 0;
		dstrect = &fulldst;
	}
	assert(srcx >= 0 && srcy >= 0);
	assert(dstrect->y >= 0 && dstrect->x >= 0);
	uint8_t *srcpixels;
	srcpixels = (uint8_t *) src->pixels + srcy * src->pitch + srcx;
	uint8_t *dstpixels;
	dstpixels = (uint8_t *) dst->pixels + dstrect->y * dst->pitch + dstrect->x;

	assert(dstrect->x >= 0 && dstrect->y >= 0);
	if (w + dstrect->x > dst->w)
		w = dst->w - dstrect->x;
	if (h + dstrect->y > dst->h)
		h = dst->h - dstrect->y;
	
	dstrect->w = w;
	dstrect->h = h;
	while (h--) {
		memcpy(dstpixels, srcpixels, w);
		dstpixels += dst->pitch;
		srcpixels += src->pitch;
	}

}

void SDL_FillRect(SDL_Surface *dst, SDL_Rect *rect, uint32_t color) {
	assert(dst);
	assert(color <= 0xff);

	/* TODO: Fill the rectangle area described by ``dstrect''
	 * in surface ``dst'' with color ``color''. If dstrect is
	 * NULL, fill the whole surface.
	 */
	uint8_t *pixels;

	/* Perform software fill */
	if (rect == NULL) {
		rect = &dst->clip_rect;
	}
	
	pixels = (uint8_t *) dst->pixels + rect->y * dst->pitch + rect->x;
	assert(rect->y >= 0 && rect->x >= 0);

	int w, h;
	w = rect->w;
	h = rect->h;

	if (w + rect->x > dst->w)
		w = dst->w - rect->x;
	if (h + rect->y > dst->h)
		h = dst->h - rect->y;

	rect->w = w;
	rect->h = h;
	while (h--) {
		memset(pixels, color, w);
		pixels += dst->pitch;
	}

}

void SDL_UpdateRect(SDL_Surface *screen, int x, int y, int w, int h) {
	assert(screen);
	assert(screen->pitch == 320);
		
	if(screen->flags & SDL_HWSURFACE) {
		if(x == 0 && y == 0) {
			/* Draw FPS */
			vmem = VMEM_ADDR;
			char buf[80];
			sprintf(buf, "%dFPS", get_fps());
			draw_string(buf, 0, 0, 10);
		}
		return;
	}

	/* TODO: Copy the pixels in the rectangle area to the screen. */
	assert(0);
	if ( w == 0 )
		w = screen->w;
	if ( h == 0 )
		h = screen->h;
	if ( (int)(x+w) > screen->w )
		return;
	if ( (int)(y+h) > screen->h )
		return;
	uint8_t *pixels;
	pixels = (uint8_t *) screen->pixels + y * screen->pitch + x;
	
	uint8_t *vmem2 = VMEM_ADDR + y * screen->pitch + x;
	
	while (h--) {
		memcpy(vmem2, pixels, w);
		pixels += screen->pitch;
		vmem2 += screen->pitch;
	}
}

void SDL_SetPalette(SDL_Surface *s, int flags, SDL_Color *colors, 
		int firstcolor, int ncolors) {
	assert(s);
	assert(s->format);
	assert(s->format->palette);
	assert(firstcolor == 0);

	if(s->format->palette->colors == NULL || s->format->palette->ncolors != ncolors) {
		if(s->format->palette->ncolors != ncolors && s->format->palette->colors != NULL) {
			/* If the size of the new palette is different 
			 * from the old one, free the old one.
			 */
			free(s->format->palette->colors);
		}

		/* Get new memory space to store the new palette. */
		s->format->palette->colors = malloc(sizeof(SDL_Color) * ncolors);
		assert(s->format->palette->colors);
	}

	/* Set the new palette. */
	s->format->palette->ncolors = ncolors;
	memcpy(s->format->palette->colors, colors, sizeof(SDL_Color) * ncolors);

	if(s->flags & SDL_HWSURFACE) {
		/* TODO: Set the VGA palette by calling write_palette(). */
		//assert(0);
		write_palette((void*)colors, ncolors);
	}
}

/* ======== The following functions are already implemented. ======== */

void SDL_SoftStretch(SDL_Surface *src, SDL_Rect *scrrect, 
		SDL_Surface *dst, SDL_Rect *dstrect) {
	assert(src && dst);
	int x = (scrrect == NULL ? 0 : scrrect->x);
	int y = (scrrect == NULL ? 0 : scrrect->y);
	int w = (scrrect == NULL ? src->w : scrrect->w);
	int h = (scrrect == NULL ? src->h : scrrect->h);

	assert(dstrect);
	if(w == dstrect->w && h == dstrect->h) {
		/* The source rectangle and the destination rectangle
		 * are of the same size. If that is the case, there
		 * is no need to stretch, just copy. */
		SDL_Rect rect;
		rect.x = x;
		rect.y = y;
		rect.w = w;
		rect.h = h;
		SDL_BlitSurface(src, &rect, dst, dstrect);
	}
	else {
		/* No other case occurs in NEMU-PAL. */
		assert(0);
	}
}

SDL_Surface* SDL_CreateRGBSurface(uint32_t flags, int width, int height, int depth,
		uint32_t Rmask, uint32_t Gmask, uint32_t Bmask, uint32_t Amask) {
	SDL_Surface *s = malloc(sizeof(SDL_Surface));
	assert(s);
	s->format = malloc(sizeof(SDL_PixelFormat));
	assert(s);
	s->format->palette = malloc(sizeof(SDL_Palette));
	assert(s->format->palette);
	s->format->palette->colors = NULL;

	s->format->BitsPerPixel = depth;

	s->flags = flags;
	s->w = width;
	s->h = height;
	s->pitch = (width * depth) >> 3;
	s->pixels = (flags & SDL_HWSURFACE ? (void *)VMEM_ADDR : malloc(s->pitch * height));
	assert(s->pixels);

	return s;
}

SDL_Surface* SDL_SetVideoMode(int width, int height, int bpp, uint32_t flags) {
	return SDL_CreateRGBSurface(flags,  width, height, bpp,
			0x000000ff, 0x0000ff00, 0x00ff0000, 0xff000000);
}

void SDL_FreeSurface(SDL_Surface *s) {
	if(s != NULL) {
		if(s->format != NULL) {
			if(s->format->palette != NULL) {
				if(s->format->palette->colors != NULL) {
					free(s->format->palette->colors);
				}
				free(s->format->palette);
			}

			free(s->format);
		}
		
		if(s->pixels != NULL) {
			free(s->pixels);
		}
		free(s);
	}
}

